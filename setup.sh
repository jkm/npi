#!/bin/sh


CHROOT_DIR="tmp"
MOUNT_DIR="/mnt"

ALPINE_ARCH="armv7"
ALPINE_MAJOR_VERSION="3.15"
ALPINE_MINOR_VERSION="0"
ALPINE_ROOTFS_FILE="alpine-minirootfs-${ALPINE_MAJOR_VERSION}.${ALPINE_MINOR_VERSION}-${ALPINE_ARCH}.tar.gz"


# Detach all mounted images
losetup -D

# Remove old image
rm -f npi.img

# Setup image file
dd if=/dev/zero of=npi.img bs=512 count=65536 status=none

# Attach image to loop device
losetup -P /dev/loop0 npi.img

# Partition disk image
sfdisk -q /dev/loop0 < disk/layout

# Make filesystems
mkfs.vfat /dev/loop0p1
mkfs.ext4 -q /dev/loop0p2

# Boot partition
mount /dev/loop0p1 $MOUNT_DIR
cp -r boot/* $MOUNT_DIR
mkimage -C none -A arm -T script -d $MOUNT_DIR/boot.cmd $MOUNT_DIR/boot.scr
find $MOUNT_DIR | xargs touch -h
umount /dev/loop0p1

# Mount rootfs volume
mount /dev/loop0p2 $MOUNT_DIR

# Unpack Alpine Linux rootfs
tar xf $ALPINE_ROOTFS_FILE -C $MOUNT_DIR

# Create directories
mkdir $MOUNT_DIR/root/.ssh
mkdir $MOUNT_DIR/etc/dropbear
mkdir $MOUNT_DIR/var/lib/eiwd

# Copy necessary files
cp etc/inittab $MOUNT_DIR/etc/inittab
cp etc/profile $MOUNT_DIR/etc/profile
cp etc/init.d/rcS $MOUNT_DIR/etc/init.d/rcS

# Copy binaries
cp $CHROOT_DIR/usr/bin/eiwd $MOUNT_DIR/usr/bin/eiwd

# Copy .psk files
if [ -f /var/lib/eiwd/*.psk ]; then
  cp /var/lib/eiwd/*.psk $MOUNT_DIR/var/lib/eiwd
fi

# Copy authorized_keys file
if [ -f $HOME/.ssh/authorized_keys ]; then
  cp $HOME/.ssh/authorized_keys $MOUNT_DIR/root/.ssh
fi

# Set hostname
echo npi > $MOUNT_DIR/etc/hostname

# Set default DNS server
echo "nameserver 1.1.1.1" > $MOUNT_DIR/etc/resolv.conf

# Update packages
chroot $MOUNT_DIR apk -q update

# Install packages
chroot $MOUNT_DIR apk -q add \
			dropbear \
			e2fsprogs \
			e2fsprogs-extra \
			eudev \
			htop \
			iw

# Upgrade packages
chroot $MOUNT_DIR apk -q upgrade

# Touch all files
find $MOUNT_DIR | xargs touch -h

# Umount rootfs volume
umount /dev/loop0p2

# Flash bootloader
dd if=boot/u-boot-sunxi-with-spl.bin of=/dev/loop0 bs=1024 seek=8 status=none

# Detach image from loop device
losetup -d /dev/loop0

# Copy image to given block device
if [ ! -z $1 ]; then
  dd if=npi.img of=$1 bs=4M status=none
fi
