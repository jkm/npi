#!/bin/sh


ARCH="armv7"
CHROOT_DIR="tmp"

ROOTFS_MAJOR_VERSION="3.16"
ROOTFS_MINOR_VERSION="2"
ROOTFS_VERSION="${ROOTFS_MAJOR_VERSION}.${ROOTFS_MINOR_VERSION}"
ROOTFS_FILE="alpine-minirootfs-${ROOTFS_VERSION}-${ARCH}.tar.gz"
ROOTFS_URL="https://dl-cdn.alpinelinux.org/alpine/v${ROOTFS_MAJOR_VERSION}/releases/${ARCH}/${ROOTFS_FILE}"


# Download Alpine Linux rootfs
if [ ! -f ${ROOTFS_FILE} ]; then
  wget -q $ROOTFS_URL
  wget -q $ROOTFS_URL.sha256
fi

# Verify checksum
sha256sum --quiet -c $ROOTFS_FILE.sha256

# Create chroot directory
mkdir $CHROOT_DIR

# Unpack mkroot rootfs
tar xf $ROOTFS_FILE -C $CHROOT_DIR

# Change ownership
chown -R root:root $CHROOT_DIR

# Create bin/tmp directory
mkdir -p bin/tmp

# Mount pseudo filesystems
mount --bind /dev $CHROOT_DIR/dev
mount --bind /dev/pts $CHROOT_DIR/dev/pts
mount --bind /sys $CHROOT_DIR/sys
mount --bind /proc $CHROOT_DIR/proc

# Set DNS server
echo "nameserver 1.1.1.1" > $CHROOT_DIR/etc/resolv.conf

# Set hostname
chroot $CHROOT_DIR hostname rpi

# Update packages
chroot $CHROOT_DIR apk -q update

# Install packages
chroot $CHROOT_DIR apk -q add cmake make
