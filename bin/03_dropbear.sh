#!/bin/sh


CHROOT_DIR="tmp"

VERSION="2022.82"
FILE="dropbear-${VERSION}.tar.bz2"
URL="https://matt.ucc.asn.au/dropbear/releases/${FILE}"
FLAGS="--enable-static --disable-shadow --disable-syslog --disable-zlib --disable-utmp --disable-utmpx --disable-wtmp --disable-wtmpx"


# Get dropbear archive
chroot $CHROOT_DIR wget -q $URL

# Unpack dropbear archive
chroot $CHROOT_DIR tar xf $FILE

# Build dropbear
chroot $CHROOT_DIR dropbear-$VERSION/configure -q $FLAGS
chroot $CHROOT_DIR make PROGRAMS="dropbear"

# Copy dropbear binary
cp $CHROOT_DIR/dropbear bin/tmp
