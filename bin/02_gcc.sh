#!/bin/sh


ARCH="armv7l"
CHROOT_DIR="tmp"

NAME="${ARCH}-linux-musleabihf-native"
FILE="${NAME}.tgz"
URL="https://musl.cc/${FILE}"


# Download toolchain
chroot $CHROOT_DIR wget -q $URL

# Unpack toolchain
chroot $CHROOT_DIR tar xf $FILE -C /usr/lib

# Change toolchain directory ownership
chroot $CHROOT_DIR chown -R root:root /usr/lib/$NAME

# Create symlinks
chroot $CHROOT_DIR mkdir -p /usr/local/bin
chroot $CHROOT_DIR find /usr/lib/$NAME/bin -executable -exec ln -s {} /usr/local/bin \;

# Remove toolchain archive
chroot $CHROOT_DIR rm $FILE
