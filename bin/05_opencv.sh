#!/bin/sh


CHROOT_DIR="tmp"

VERSION="4.6.0"
FILE="${VERSION}.zip"
URL="https://github.com/opencv/opencv/archive/${FILE}"
FLAGS="-DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_EXAMPLES=OFF"

# Get OpenCV archive
chroot $CHROOT_DIR wget -q $URL

# Unpack OpenCV archive
chroot $CHROOT_DIR unzip -q $FILE

# Build OpenCV
chroot $CHROOT_DIR mkdir build
chroot $CHROOT_DIR cmake $FLAGS -B build opencv-$VERSION
chroot $CHROOT_DIR make -C build

# Copy OpenCV libs
cp -r tmp/build/lib bin/tmp
