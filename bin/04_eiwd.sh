#!/bin/sh


CHROOT_DIR="tmp"

FILE="master.tar.gz"
URL="https://codeberg.org/jkm/eiwd/archive/${FILE}"


# Get eiwd archive
chroot $CHROOT_DIR wget -q $URL

# Unpack eiwd archive
chroot $CHROOT_DIR tar xf $FILE

# Compile eiwd binary
chroot $CHROOT_DIR make -C eiwd
chroot $CHROOT_DIR make -C eiwd install

# Copy eiwd binary
cp $CHROOT_DIR/eiwd/eiwd bin/tmp
