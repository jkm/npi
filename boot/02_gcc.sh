#!/bin/sh


CHROOT_DIR="tmp"

VERSION="10.3-2021.07"
NAME="gcc-arm-${VERSION}-aarch64-arm-none-linux-gnueabihf"
FILE="${NAME}.tar.xz"
URL="https://developer.arm.com/-/media/Files/downloads/gnu-a/${VERSION}/binrel/${FILE}"


# Download GCC toolchain
chroot $CHROOT_DIR wget -q $URL

# Unpack GCC toolchain
chroot $CHROOT_DIR tar xf $FILE -C /usr/local
chroot $CHROOT_DIR ln -s /usr/local/$NAME /usr/lib/arm-none-linux-gnueabihf
