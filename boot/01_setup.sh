#!/bin/sh


CHROOT_DIR="tmp"

VERSION="bullseye"
FILE="rootfs.tar.xz"
URL="https://raw.githubusercontent.com/debuerreotype/docker-debian-artifacts/dist-arm64v8/${VERSION}/slim/${FILE}"


# Download Debian rootfs
if [ ! -f $FILE ]; then
  wget -q $URL
  wget -q $URL.sha256
fi

# Verify checksum
if ! grep -q $FILE $FILE.sha256; then
  sed -i '1 s/$/  '"$FILE"'/' $FILE.sha256
fi

sha256sum -c $FILE.sha256
if [ $? -ne 0 ]; then
  echo "Malformed file"
  exit 1
fi

# Create chroot directory
mkdir $CHROOT_DIR

# Unpack Debian rootfs
tar xf $FILE -C $CHROOT_DIR

# Mount pseudo filesystems
mount --bind /dev $CHROOT_DIR/dev
mount --bind /dev/pts $CHROOT_DIR/dev/pts
mount --bind /sys $CHROOT_DIR/sys
mount --bind /proc $CHROOT_DIR/proc

# Set DNS server
echo "nameserver 1.1.1.1" > $CHROOT_DIR/etc/resolv.conf

# Set hostname
chroot $CHROOT_DIR hostname rpi

# Install packages
chroot $CHROOT_DIR apt update
chroot $CHROOT_DIR apt install bc bison flex libssl-dev locales lz4 lzma make patch ca-certificates wget wireless-regdb xz-utils -y
