#!/bin/sh


CHROOT_DIR="tmp"

VERSION="v2022.07"
FILE="u-boot-${VERSION}.tar.gz"
URL="https://source.denx.de/u-boot/u-boot/-/archive/${VERSION}/${FILE}"


# Install packages
chroot $CHROOT_DIR apt update
chroot $CHROOT_DIR apt install python3-dev python3-setuptools swig -y

# Get U-Boot archive
chroot $CHROOT_DIR wget -q $URL

# Unpack the U-Boot archive
chroot $CHROOT_DIR tar xf $FILE

# Make defconfig
chroot $CHROOT_DIR make -C u-boot-${VERSION} nanopi_neo_air_defconfig

# Compile U-Boot
chroot $CHROOT_DIR make -C u-boot-${VERSION} CROSS_COMPILE=/usr/lib/arm-none-linux-gnueabihf/bin/arm-none-linux-gnueabihf-

# Copy U-Boot binary
cp $CHROOT_DIR/u-boot-${VERSION}/u-boot-sunxi-with-spl.bin boot

# Generate U-Boot image
./$CHROOT_DIR/u-boot-${VERSION}/tools/mkimage -C none -A arm -T script -d boot/boot.cmd boot/boot.scr
