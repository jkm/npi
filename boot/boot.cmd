setenv fdt_addr 0x43000000
setenv kernel_addr 0x45000000
setenv bootargs "root=/dev/mmcblk1p2 rw rootwait rootfstype=ext4 console=ttyS0,115200"

load mmc 0 ${fdt_addr} ${fdtfile}
load mmc 0 ${kernel_addr} zImage

fdt addr ${fdt_addr}
fdt resize 65536

bootz ${kernel_addr} - ${fdt_addr}

# Recompile with:
# mkimage -C none -A arm -T script -d /boot/boot.cmd /boot/boot.scr
