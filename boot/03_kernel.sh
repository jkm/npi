#!/bin/sh


CHROOT_DIR="tmp"

VERSION="5.10.144"
FILE="linux-${VERSION}.tar.xz"
URL="https://cdn.kernel.org/pub/linux/kernel/v5.x/${FILE}"


# Get Linux kernel archive
chroot $CHROOT_DIR wget -q $URL

# Unpack the kernel archive
chroot $CHROOT_DIR tar xf $FILE

# Patch the kernel
chroot $CHROOT_DIR patch -d linux-$VERSION -p0 < kernel/linux.patch

# Copy AP6212 firmware
mkdir -p $CHROOT_DIR/lib/firmware/brcm
cp -r kernel/firmware/brcm $CHROOT_DIR/lib/firmware

# Copy .config file
cp kernel/.config $CHROOT_DIR/linux-${VERSION}

# Compile the kernel image and device tree
chroot $CHROOT_DIR make -C linux-${VERSION} ARCH=arm CROSS_COMPILE=/usr/lib/arm-none-linux-gnueabihf/bin/arm-none-linux-gnueabihf- zImage
chroot $CHROOT_DIR make -C linux-${VERSION} ARCH=arm CROSS_COMPILE=/usr/lib/arm-none-linux-gnueabihf/bin/arm-none-linux-gnueabihf- dtbs

# Copy new .config file
cp $CHROOT_DIR/linux-${VERSION}/.config kernel

# Copy artifacts
cp $CHROOT_DIR/linux-${VERSION}/arch/arm/boot/zImage boot
cp $CHROOT_DIR/linux-${VERSION}/arch/arm/boot/dts/sun8i-h3-nanopi-neo-air.dtb boot
