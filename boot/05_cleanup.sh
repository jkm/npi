#!/bin/sh


CHROOT_DIR="tmp"


# Umount chroot psuedo filesystems
umount $CHROOT_DIR/dev/pts
umount $CHROOT_DIR/dev
umount $CHROOT_DIR/sys
umount $CHROOT_DIR/proc

# Remove chroot
rm -rf $CHROOT_DIR
